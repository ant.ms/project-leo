///
/// Program-Name:   Project Leo
/// Filename:       Project_Leo.ino
/// Description:    Project Leo is a animated heart I made for my boyfriend as a Valentines-Day gift
/// Author:         Yanik Ammann
/// Last Changed:   14.02.2020 by Yanik Ammann
/// Todo:           - Give Julien some hugs
/// Dependencied:   FastLed.h
///
/// Notes:          For some reason the hue of the leds is completely off, maybe selected the wrong model
///                 or something, anyways, it requires a lot of trial and error to get the right colors
///


#include "FastLED.h"
 
// How many leds in your strip?
#define ledAmmount 17
// What (digital) pin are the leds attached too
#define ledPin 5
// switch pin (analog because.... it just didn't work otherwise)
#define SwitchButtonPin A1
// how many animations are existant in this script (please keep updated)
#define animationAmmount 5
 
CRGB leds[ledAmmount];

int brightness = 160;
int mode = 0;
 
void setup() { 
    // set type of leds to use
    FastLED.addLeds<WS2812B, ledPin, RGB>(leds, ledAmmount);
    FastLED.setBrightness(brightness);
    pinMode(SwitchButtonPin, INPUT);

    // start debugging (only if PC is connected through a (USB-Mikro-B (or whatever that is called) cable directly to the arduino)
    Serial.begin(9600);  
    Serial.println("--- Start Serial Monitor SEND_RCVE ---\n"); 
}

bool readValue() {
  if (analogRead(SwitchButtonPin) > 1000) {
    // counts through the animations
    mode++;
    // starts again at the first one if all the other ones are already been gone through (creates a loop)
    if (mode >= animationAmmount)
      mode = 0;

    // turns off the LEDs
    for(int i=0; i < ledAmmount; i++)
        leds[i] = CHSV(0, 255, brightness);
    FastLED.clear ();

    // logs to console
    Serial.print("Button pressed, mode is ");
    Serial.println(mode);

    // wait until button is let loose, then wait some more
    while (analogRead(SwitchButtonPin) > 900) {}
    delay(400);
    return true;
  }
  return false;
}
 

void loop()
{
  // detect input
  readValue();
  
  // set brightness again because some animations may rewrite it
  FastLED.setBrightness(brightness);

  // start the individual LEDs
  if (mode == 0) {
    animationOne();
  } else if (mode == 1) {
    animationTwo();
  } else if (mode == 2) {
    animationThree();
  } else if (mode == 3) {
    animationFour();
  } else if (mode == 4) {
    animationFive();
  }
}


bool animationOne() {
  for(int i=0; i<ledAmmount/2+1; i++){
      leds[ledAmmount/2 + i] = CHSV(96, 255, brightness);
      leds[ledAmmount/2 - i] = CHSV(96, 255, brightness);
      FastLED.show();
      delay(100);
      // exit if animation changed
      if (readValue() == true)
        return true;
  }
  delay(100);
  for(int i=0; i<ledAmmount/2+1; i++){
      leds[ledAmmount/2 + i] = CHSV(160, 255, 0);
      leds[ledAmmount/2 - i] = CHSV(160, 255, 0);
      FastLED.show();
      delay(100);
      // exit if animation changed
      if (readValue() == true)
        return true;
   }
  return false;
}

bool animationTwo() {
  for(int i=0; i<ledAmmount; i++){
        leds[i] = CHSV(160, 255, brightness);
        FastLED.show();
        delay(50);
        leds[i] = CHSV(0,0,0);
        FastLED.show();
        // exit if animation changed
        if (readValue() == true)
          return true;
   }
  return false;
}

bool animationThree() {
  int ammountOfRuns = 120;
  // rainbow animation, only for 17 LED's

  // set hue (pride flag)
  leds[6-1].setHue(96);
  leds[7-1].setHue(96);
  leds[8-1].setHue(96);
  leds[9-1].setHue(96);
  leds[10-1].setHue(96);
  leds[11-1].setHue(96);
  leds[5-1].setHue(80);
  leds[12-1].setHue(80);
  leds[4-1].setHue(160);
  leds[13-1].setHue(255);
  leds[4-1].setHue(255);
  leds[14-1].setHue(255);
  leds[3-1].setHue(255);
  leds[2-1] = CRGB::Blue;
  leds[15-1] = CRGB::Blue;
  leds[16-1].setHue(110);
  leds[1-1].setHue(110);
  leds[17-1].setHue(110);

  // change brightness (up)
  for (int j=0; j < ammountOfRuns; j++) {
    FastLED.setBrightness(map(j, 0, ammountOfRuns, 10, 230));
    delay(50);
    FastLED.show();
     // exit if animation changed
    if (readValue() == true)
      return true;
  }
  // change brightness (down)
  for (int j=ammountOfRuns; j > 0; j--) {
    FastLED.setBrightness(map(j, 0, ammountOfRuns, 10, 230));
    delay(50);
    FastLED.show();
    // exit if animation changed
    if (readValue() == true)
      return true;
  }
  return false;
}

bool animationFour() {
  // change hue (color of LEDs)
  for (int hue = 0; hue < 255; hue += 2) {
    for(int i=0; i < ledAmmount; i++){
        leds[i].setHue(hue);
    }
    delay(100);
    FastLED.show();
    // exit if animation changed
    if (readValue() == true)
      return true;
  }
  for (int hue = 255; hue > 0; hue -= 2) {
    for(int i=0; i < ledAmmount; i++){
        leds[i].setHue(hue);
    }
    delay(100);
    FastLED.show();
    // exit if animation changed
    if (readValue() == true)
      return true;
  }
  return false;
}

bool animationFive() {
  // clears LEDs (sets them to dark)
  FastLED.clear();
  int ammountOfRuns = 40;
  int ammountOfLeds = 3;

  // sets color to red for some of them (the rest stay dark)
  for (int i = 0; i < ammountOfLeds; i++)
    leds[random(0, ledAmmount - 1)].setHue(96);

  // increase brightness
  for (int j=0; j < ammountOfRuns; j++) {
    FastLED.setBrightness(map(j, 0, ammountOfRuns, 15, 120));
    delay(50);
    FastLED.show();
    // exit if animation changed
    if (readValue() == true)
      return true;
  }
  // reduce brightness
  for (int j=ammountOfRuns; j > 0; j--) {
    FastLED.setBrightness(map(j, 0, ammountOfRuns, 15, 120));
    delay(50);
    FastLED.show();
    // exit if animation changed
    if (readValue() == true)
      return true;
  }
  return false;
}
